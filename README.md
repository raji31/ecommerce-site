### Intro 
    This project is a demonstration of a sample ecommerce project that will allow users to do the following things 
        - 1. Allow users to see top trending products based on search history 
        - 2. Allow users to save products to a card 
        - 3. Users will see price and review history from various sites such as Amazon, Walmart, and Target 
        - 4. Each product will have their own individual page where users can se see a description, image of the product, reviews, and 
             price trends 
        - 5. Users can filter their searches base on price, product category, and ratings 
        - 6. Allow users to create simple profile setups by creating their own nicknames, upload their images, and account management 
    